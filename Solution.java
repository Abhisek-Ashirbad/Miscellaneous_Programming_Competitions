/*
Program expects to evaluate the following.
I\P::
i)first line no of testcases.
ii)second line no of queries.
iii)the query consisting of numbers separated by spaces.
O\P::
Evaluate the numbers in the query by converting the numbers to their respective binary counterpart and examine the no of 1s in them. 
Based on similar no of 1s, the original numbers are grouped.
Then the final result is the minimum from each group.

Example:
I\P:
2
3
4 8 13
4
1 6 4 9

O\P:
4 13
1 6
*/

class Solution 
{
    /**
     * @param args the command line arguments
     */
    
    public static int t;
    public static int n;
    public static String input;
    public static String result;
    public static String[] strArray;
    public static int[] intArray;
    public static List<String> finalList = new ArrayList<>();
    public static List<String> binaryList = new ArrayList<>();
    public static List<Integer> countList = new ArrayList<>();
    
    public static String binary(int x)
    {
        int a;
        String s="";
        while(x>0)
        {
            a=x%2;            
            s=a+""+s;
            x=x/2;
        }
        return s;
    }
    
    public static int countOfOne(String x)
    {
        int count=0;
        for(int i=0;i<x.length();i++)
        {
            if(x.charAt(i)=='1')
            {
                count++;
            }
        }
        return count;
    }
    
    public static void main(String[] args) 
    {

        try (Scanner s = new Scanner(System.in).useDelimiter("\\n")) {
            
            t = s.nextInt();
            for(int x=0;x<t;x++)
            {
                n=s.nextInt();
                if(s.hasNext())
                    input=s.next();
                
                strArray = input.split(" ");
                intArray = new int[strArray.length];
                
                for(int i=0;i<intArray.length;i++)
                {
                    String ref = strArray[i];
                    intArray[i] = Integer.parseInt(ref);
                }
                
                binaryList = new ArrayList<>(intArray.length);
                for(int i: intArray)
                {
                    binaryList.add(binary(i));
                }
                
                countList = new ArrayList<>(binaryList.size());
                binaryList.forEach((i) -> {
                    countList.add(countOfOne(i));
                });
                
                Map<Integer, List<Integer>> indexes = new HashMap<>();
                for (int i = 0; i < countList.size(); i++)
                {
                    indexes.computeIfAbsent(countList.get(i), c -> new ArrayList<>()).add(i);
                }
                
                Collection<List<Integer>> indexList = indexes.values();
                
                List<Object> minList = new ArrayList<>();
                
                Iterator<List<Integer>> itr = indexList.iterator();
                while(itr.hasNext())
                {
                    Iterator<Integer> litr = itr.next().iterator();
                    List<Integer> finalSubList = new ArrayList<>();
                    while(litr.hasNext())
                    {
                        int temp = litr.next();
                        finalSubList.add(intArray[temp]);
                    }
                    Object o = Collections.min(finalSubList);
                    minList.add(o);
                    result= String.valueOf(minList);
                }
                finalList.add(result);
            }

        }
    
        finalList.forEach((String string) -> {
            System.out.println(string.replace(",", "").replace("[","").replace("]",""));
        });
        
    }
    
}